#!/bin/sh
# Just doing the above line to get highlighting.

function ogg2x() {
    ffmpeg -i $1 -filter:a "atempo=2.0" -c:a libvorbis -q 9 2x$1_.ogg
}
# Converts audio to 2x speed.

alias update='sudo apt update && sudo apt full-upgrade -y'
# Updates the computer in one command.

alias nightmode='echo 5 | sudo tee > /sys/class/backlight/intel_backlight/brightness'
# Changes backlight on laptops to really dark.

#alias gitemall='find . -mindepth 1 -maxdepth 1 -type d -print -exec git -C {} pull \;'
# Does a pull on every subdirectory of current dir.

alias scree='screen -Ux'
# Open existing screen quicker.

#alias sudo='sudo '
#alias shutdown='echo nope'
# On a remote server that you don't want to accidentally shutdown.

alias resize='mogrify -resize "1200x1200>" -quality 96 -sharpen 0x.5 *.jpg'
alias resizeJPG='mogrify -resize "1200x1200>" -quality 96 -sharpen 0x.5 *.JPG'
# Resizes with a very slight sharpening, like Photoshop.
